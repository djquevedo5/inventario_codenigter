<?php

defined('BASEPATH') or exit('No direct script access allowed');



$lang['sale_added']       = 'Estimado {customer}, su pedido (Ref. {sale_reference}) ha sido recibido, proceda al pago por un monto de {grand_total}. Gracias';
$lang['payment_received'] = 'Estimado {customer}, su pago (Ref. {payment_reference}, Amt: {amount}) ha sido recibido, procesaremos su pedido en breve. Gracias';
$lang['delivering']       = 'Estimado {customer}, Estamos entregando su pedido (Ref. {delivery_reference}).';
$lang['delived']          = 'Estimado {clientesr}, su pedido (Ref. {sale_reference}) ha sido recibido por {received_by}.';
